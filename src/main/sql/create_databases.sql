/* USAGE: psql -U postgres -W < src/main/sql/create_databases.sql */

SET client_encoding = 'UTF8';

DROP DATABASE abawebapps;
DROP USER abawebapps;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;
COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';

CREATE USER abawebapps WITH SUPERUSER PASSWORD 'tO4exuljMUUC0cd0';
CREATE DATABASE abawebapps WITH TEMPLATE = template0 OWNER = abawebapps;

\connect abawebapps
CREATE EXTENSION IF NOT EXISTS hstore;
