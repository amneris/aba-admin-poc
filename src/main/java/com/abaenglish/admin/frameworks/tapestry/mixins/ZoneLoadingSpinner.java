package com.abaenglish.admin.frameworks.tapestry.mixins;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

public class ZoneLoadingSpinner {

	@Inject
	private JavaScriptSupport jsSupport;

	public void afterRender() {
		jsSupport.require("aba/spinner-zone-overlay");
	}


}
