package com.abaenglish.admin.frameworks.tapestry.modules;

import com.abaenglish.admin.frameworks.tapestry.SymbolConstants;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.services.ApplicationDefaults;
import org.apache.tapestry5.ioc.services.SymbolProvider;

public class DevelopmentModule {

	@Contribute(SymbolProvider.class)
	@ApplicationDefaults
	public static void provideSymbols(MappedConfiguration<String, Object> configuration) {

//		configuration.add(TynamoJpaSymbols.ELASTICSEARCH_HOME, ".");
//		configuration.add(TynamoJpaSymbols.ELASTICSEARCH_HTTP_ENABLED, true);

		final String host = "localhost";
		final String database = "ascandroli";
		final String user = "ascandroli";
		final int port = 5432;
		final String password = "";

		configuration.add(SymbolConstants.DB_URL, buildURL(host, port, database));
		configuration.add(SymbolConstants.DB_USERNAME, user);
		configuration.add(SymbolConstants.DB_PASSWORD, password);
	}

	public static String buildURL(String host, int port, String database) {
		return "jdbc:postgresql://" + host + ":" + port + "/" + database;// + "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
	}
}
