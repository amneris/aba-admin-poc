package com.abaenglish.admin.interfaces.rest;

import org.apache.tapestry5.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/echo")
public class EchoResource {

	@GET
	@Path("/{message}")
	@Produces("application/json")
	public Response echo(@PathParam("message") String message) {
		return Response.status(200).entity(new JSONObject("message", message).toCompactString()).build();
	}

}