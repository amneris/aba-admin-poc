package com.abaenglish.admin.interfaces;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

public interface GenericResource {


	String APPLICATION_JSON = "application/json";

	@GET
	@Path("/{className}")
	@Produces(APPLICATION_JSON)
	List<Object> get(@PathParam("className") String className);

	@GET
	@Path("/{className}/{id}")
	@Produces(APPLICATION_JSON)
	Object getById(@PathParam("className") String className, @PathParam("id") UUID id);

	@POST
	@Path("/{className}")
	@Produces(APPLICATION_JSON)
	@Consumes(APPLICATION_JSON)
	Response post(@PathParam("className") String className, String domainObject);
}
