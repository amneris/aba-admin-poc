package com.abaenglish.admin.frameworks.tynamo;

public interface ElasticSearchService {
	void rebuildIndex(Class entityType);
}
