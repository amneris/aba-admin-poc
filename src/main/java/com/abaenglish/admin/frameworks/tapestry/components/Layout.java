package com.abaenglish.admin.frameworks.tapestry.components;

import com.abaenglish.admin.frameworks.tapestry.pages.Unauthorized;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.apache.tapestry5.Asset;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.func.F;
import org.apache.tapestry5.func.Predicate;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.tynamo.descriptor.TynamoClassDescriptor;
import org.tynamo.security.internal.services.LoginContextService;
import org.tynamo.security.services.SecurityService;
import org.tynamo.services.DescriptorService;
import org.tynamo.util.DisplayNameUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;

@Import(module = "bootstrap/dropdown")
public class Layout {

	@Inject
	private ComponentResources resources;

	@Inject
	private PageRenderLinkSource linkSource;

	/**
	 * The page title, for the <title> element
	 */
	@Property
	@Parameter(required = true)
	private String title;

	@Property
	@Inject
	@Symbol(org.apache.tapestry5.SymbolConstants.APPLICATION_VERSION)
	private String appVersion;


	public String getClassForPageName(String pageName) {
		return resources.getPageName().equalsIgnoreCase(pageName) ? "active" : null;
	}

	@Inject
	@Property
	private SecurityService securityService;

	@Inject
	private LoginContextService loginContextService;

	public String onActionFromTynamoLoginLink()
	{
		removeSavedRequest();
		return loginContextService.getLoginPage();
	}

	public Object onActionFromTynamoLogoutLink()
	{
		securityService.getSubject().logout();
		return linkSource.createPageRenderLink(Unauthorized.class);
	}

	private void removeSavedRequest()
	{
		Subject subject = securityService.getSubject();
		if (subject != null)
		{
			subject.getSession().removeAttribute(WebUtils.SAVED_REQUEST_KEY);
		}
	}

	@Inject
	private Locale locale;

	@Inject
	private AssetSource assetSource;

	public Asset getLogo(){
		return assetSource.getContextAsset("default.png", locale);
	}

	@Inject
	private DescriptorService descriptorService;

	@Inject
	private Messages messages;

	@Property
	private String listPageName = "List";

	@Property
	private TynamoClassDescriptor descriptorIterator;

	public List<TynamoClassDescriptor> getDisplayableDescriptors()
	{
		return F.flow(descriptorService.getAllDescriptors()).filter(new Predicate<TynamoClassDescriptor>()
		{
			public boolean accept(TynamoClassDescriptor classDescriptor)
			{
				return !classDescriptor.isNonVisual();
			}
		}).sort(new Comparator<TynamoClassDescriptor>()
		{
			public int compare(TynamoClassDescriptor o1, TynamoClassDescriptor o2)
			{
				return DisplayNameUtils.getDisplayName(o1, messages).compareTo(DisplayNameUtils.getDisplayName(o2, messages));
			}
		}).toList();
	}

	public String getListAllLinkMessage()
	{
		return DisplayNameUtils.getPluralDisplayName(descriptorIterator, messages);
	}


}
