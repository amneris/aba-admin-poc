package com.abaenglish.admin;

import com.abaenglish.admin.domain.model.MyDomainObject;
import com.abaenglish.admin.interfaces.GenericResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class GenericResourceTest_ {

	private final static String BASEURI = "http://localhost:8080/rest";

	@Test
	public void testHelloWorld() throws Exception {

		Client client = ClientBuilder.newClient();
		Response response = client.target(BASEURI + "/application/4711c75a-2b37-448b-8bc3-d9a83fcebfeb").request().get();
		Assert.assertEquals(response.getStatus(), 200);

		MyDomainObject application = response.readEntity(MyDomainObject.class);
		application.setId(null);

		ResteasyWebTarget target = (ResteasyWebTarget) client.target(BASEURI);
		GenericResource genericResource = target.proxy(GenericResource.class);

		ObjectMapper mapper = new ObjectMapper();

		response = genericResource.post("application", mapper.writeValueAsString(application));

		Assert.assertEquals(response.getStatus(), 201);
	}

}
