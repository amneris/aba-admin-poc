package com.abaenglish.admin.frameworks.tapestry.pages.blocks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.internal.services.PageActivationContextCollector;
import org.apache.tapestry5.services.PropertyOutputContext;

import javax.inject.Inject;
import java.util.*;

public class DisplayBlocks
{
	@Environmental
	@Property(write = false)
	private PropertyOutputContext context;

	@Property
	private Object loopIterator;

	@Property
	private int loopIndex;

	public Boolean getCheck()
	{
		return (Boolean) context.getPropertyValue();
	}

	public Object[] getShowPageContext()
	{
		return new Object[]{context.getPropertyValue().getClass(), context.getPropertyValue()};
	}

	public Object[] getLoopShowPageContext()
	{
		return new Object[]{loopIterator.getClass(), loopIterator};
	}

	public boolean isLastElement()
	{
		return loopIndex >= ((Collection) context.getPropertyValue()).size() - 1;
	}


	public String mapToJSON(){
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		String json = gson.toJson((Map) context.getPropertyValue());
		return json;
	}

	public String arrayToJSON(){
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		String json = gson.toJson((List) context.getPropertyValue());
		return json;
	}

	@Inject
	private PageActivationContextCollector contextCollector;

	public Object[] getListCollectionPageContext()
	{
		ArrayList pass = new ArrayList<>();
		pass.addAll(Arrays.asList(contextCollector.collectPageActivationContext("Show")));
		pass.add(context.getPropertyName());

		return  pass.toArray();
	}


}
