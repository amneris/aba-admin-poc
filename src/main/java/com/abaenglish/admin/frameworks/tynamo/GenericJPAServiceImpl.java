package com.abaenglish.admin.frameworks.tynamo;

import com.abaenglish.admin.frameworks.tapestry.Constants;
import com.abaenglish.admin.frameworks.tapestry.CriteriaQueryModifier;
import org.apache.tapestry5.ioc.internal.util.CollectionFactory;
import org.tynamo.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import java.util.Collections;
import java.util.List;
import java.util.Set;


public class GenericJPAServiceImpl implements GenericJPAService {

	private final CriteriaQueryModifier<?> doNothingModifier = new CriteriaQueryModifier() {
		@Override
		public void applyAdditionalConstraints(CriteriaBuilder cb, CriteriaQuery c, Root root) {
			// do nothing
		}
	};

	private EntityManager em;

	public GenericJPAServiceImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public <T> CriteriaQuery<T> criteriaQuery(Class<T> entityClass, Object... nameValuePairs) {
		return createQuery(entityClass, (CriteriaQueryModifier<T>) doNothingModifier, nameValuePairs);
	}

	@Override
	public <T> CriteriaQuery<T> createQuery(Class<T> entityClass, CriteriaQueryModifier<T> modifier, Object... nameValuePairs) {
		CriteriaBuilder cb = cb();
		CriteriaQuery<T> c = cb.createQuery(entityClass);
		Root<T> root = c.from(entityClass);
		List<Pair<Expression<?>, Object>> conditions = CollectionFactory.newList();
		for (int i = 0; i < nameValuePairs.length; i += 2) {
			String[] props = String.valueOf(nameValuePairs[i]).split(Constants.REGEX_DOT);
			Path<?> property = null;
			for (String prop : props)
				property = property == null ? root.get(prop) : property.get(prop);
			Object value = nameValuePairs[i + 1];
			conditions.add(new Pair<Expression<?>, Object>(property, value));
		}

		if (!conditions.isEmpty()) {
			if (conditions.size() == 1) {
				c = c.where(cb.equal(conditions.get(0).getKey(), conditions.get(0).getValue()));
			} else {
				Predicate[] predicates = new Predicate[conditions.size()];
				for (int i = 0; i < conditions.size(); i++) {
					Predicate eqPredicate = cb.equal(conditions.get(i).getKey(), conditions.get(i).getValue());
					predicates[i] = eqPredicate;
				}
				c = c.where(cb.and(predicates));
			}
		}
		modifier.applyAdditionalConstraints(cb, c, root);
		return c;
	}

	private CriteriaBuilder cb() {
		return em.getCriteriaBuilder();
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass, CriteriaQueryModifier<T> modifier, Object... nameValuePairs) {
		return em.createQuery(createQuery(entityClass, modifier, nameValuePairs)).getResultList();
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass, Object... nameValuePairs) {
		return em.createQuery(criteriaQuery(entityClass, nameValuePairs)).getResultList();
	}

	@Override
	public <T> T find(Class<T> entityClass, Object... nameValuePairs) {
		List<T> list = findAll(entityClass, nameValuePairs);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public void save(Object entity) {
		em.persist(entity);
	}

	@Override
	public void delete(Object entity) {
		em.remove(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> long findAllCount(Class<T> entityClass, Object... nameValuePairs) {
		@SuppressWarnings("rawtypes")
		CriteriaQuery c = criteriaQuery(entityClass, nameValuePairs);
		c.select(cb().count(getRoot(c)));
		return (Long) em.createQuery(c).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	private <T> Root<T> getRoot(CriteriaQuery<T> c) {
		return (Root<T>) c.getRoots().iterator().next();
	}

	@Override
	public <T> List<T> findIn(Class<T> beanType, final Set ids) {

		if (ids.isEmpty()) return Collections.emptyList();

		return em.createQuery(createQuery(beanType, new CriteriaQueryModifier<T>() {
			@Override
			public void applyAdditionalConstraints(CriteriaBuilder cb, CriteriaQuery<?> q, Root<T> r) {
				EntityType<T> entityType = r.getModel();
				SingularAttribute idAttr = entityType.getId(entityType.getIdType().getJavaType());
				q.where(r.get(idAttr).in(ids));
			}
		})).getResultList();
	}

}
