package com.abaenglish.admin.interfaces;

import org.codehaus.jackson.map.ObjectMapper;
import org.tynamo.services.EntityCoercerService;
import org.tynamo.services.PersistenceService;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Path("/")
public class GenericResourceImpl implements GenericResource {

	private final PersistenceService persistenceService;
	private final EntityCoercerService entityCoercerService;
	private final EntityManager em;

	public GenericResourceImpl(PersistenceService persistenceService, EntityCoercerService entityCoercerService, EntityManager em) {
		this.persistenceService = persistenceService;
		this.entityCoercerService = entityCoercerService;
		this.em = em;
	}

	@Override
	public List<Object> get(String className) {
		return persistenceService.getInstances(entityCoercerService.stringToClass(className));
	}

	@Override
	public Object getById(String className, UUID id) {
		Class clazz = entityCoercerService.stringToClass(className);
		Object domainObject = persistenceService.getInstance(clazz, id);
		if (domainObject == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return domainObject;
	}

	@Override
	public Response post(String className, String domainObject) {
		Class clazz = entityCoercerService.stringToClass(className);
		ObjectMapper mapper = new ObjectMapper();

		try {
			Object object = mapper.readValue(domainObject, clazz);

			EntityTransaction tx = em.getTransaction();
			tx.begin();
			try {
				em.persist(clazz.cast(object));
				tx.commit();
				return Response.ok().build();
			} catch (final RuntimeException e)
			{
				if (tx != null && tx.isActive())
				{
					tx.rollback();
				}
				throw e;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

}
