package com.abaenglish.admin;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class EchoResourceTest {

	private final static String BASEURI = "http://localhost:8080/rest";

	@Test
	public void testHelloWorld() throws Exception {
		Client client = ClientBuilder.newClient();
		Response response = client.target(BASEURI + "/echo/Hellow World!").request().get();
		Assert.assertEquals(response.getStatus(), 200);
		Assert.assertEquals(response.readEntity(String.class), "{\"message\":\"Hellow World!\"}");
	}

}
