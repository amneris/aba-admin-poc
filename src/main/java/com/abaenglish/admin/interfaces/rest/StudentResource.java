package com.abaenglish.admin.interfaces.rest;

import com.abaenglish.admin.domain.model.Student;
import org.tynamo.services.PersistenceService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

//@Api(value = "/rewards", description = "the REWARDS api")
@Path("/student")
public class StudentResource {

	private PersistenceService persistenceService;

	public StudentResource(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}

	@GET
	@Produces("application/json")
//	@ApiOperation("get all the rewards")
	public List<Student> getRewards() {
		return (persistenceService.getInstances(Student.class));
	}

	@POST
	@Produces("application/json")
//	@ApiOperation("saves a reward")
	public Response post(Student domainObject) {
		persistenceService.save(domainObject);
		return Response.ok().build();
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
//	@ApiOperation("gets a reward by ID")
	public Student getRewardById(@PathParam("id") UUID id) {
		Student domainObject = persistenceService.getInstance(Student.class, id);
		if (domainObject == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return domainObject;
	}

}