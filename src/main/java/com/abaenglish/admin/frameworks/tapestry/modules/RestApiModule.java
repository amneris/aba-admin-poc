package com.abaenglish.admin.frameworks.tapestry.modules;

import com.abaenglish.admin.interfaces.GenericResource;
import com.abaenglish.admin.interfaces.GenericResourceImpl;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.internal.services.RequestImpl;
import org.apache.tapestry5.internal.services.ResponseImpl;
import org.apache.tapestry5.internal.services.TapestrySessionFactory;
import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MethodAdviceReceiver;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.*;
import org.apache.tapestry5.plastic.MethodAdvice;
import org.apache.tapestry5.plastic.MethodInvocation;
import org.apache.tapestry5.services.*;
import org.tynamo.resteasy.ResteasyModule;
import org.tynamo.resteasy.ResteasyPackageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@ImportModule({
		ResteasyModule.class,
})
public class RestApiModule {

	public static void bind(ServiceBinder binder) {
		binder.bind(GenericResource.class, GenericResourceImpl.class);
	}

	/**
	 * Contributions to the RESTeasy main Application, insert all your RESTeasy singleton services here.
	 */
	@Contribute(javax.ws.rs.core.Application.class)
	public static void contributeApplication(Configuration<Object> singletons, GenericResource genericResource) {
		singletons.add(genericResource);
	}

	/**
	 * Monkey Patching RESTEasy
	 *
	 * 1) The PageRenderLinkSource needs the Request to create URLs and we need to be able to create URLs from REST services.
	 * 2) setting Access-Control-Allow-Origin
	 */
	@Advise(id = "ResteasyRequestFilter", serviceInterface = HttpServletRequestFilter.class)
	public static void monkeyPatchRESTEasy(MethodAdviceReceiver receiver,
	                                       @Inject @Symbol(SymbolConstants.CHARSET) final String applicationCharset,
	                                       final TapestrySessionFactory sessionFactory, final RequestGlobals requestGlobals) throws NoSuchMethodException {
		Method method = HttpServletRequestFilter.class.getMethod("service", HttpServletRequest.class,
				HttpServletResponse.class, HttpServletRequestHandler.class);
		receiver.adviseMethod(method, new MethodAdvice() {
			public void advise(MethodInvocation invocation) {

				HttpServletRequest servletRequest = (HttpServletRequest) invocation.getParameter(0);
				HttpServletResponse servletResponse = (HttpServletResponse) invocation.getParameter(1);

				Request request = new RequestImpl(servletRequest, applicationCharset, sessionFactory);
				Response response = new ResponseImpl(servletRequest, servletResponse);

				requestGlobals.storeRequestResponse(request, response); // 1)

				servletResponse.setHeader("Access-Control-Allow-Origin", servletRequest.getHeader("Origin")); // 2)

				invocation.proceed();
			}
		});
	}

	@Contribute(ResteasyPackageManager.class)
	public static void resteasyPackageManager(Configuration<String> configuration)
	{
		configuration.add("com.abaenglish.admin.interfaces.rest");
	}

}
