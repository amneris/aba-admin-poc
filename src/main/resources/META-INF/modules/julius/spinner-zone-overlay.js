/**
 * Shows Ajax loading spinner while waiting for results
 */

(function () {
    define(["t5/core/dom", "t5/core/events"], function (dom, events) {
        var createOverlay = function (elt) {
            dom.body.prepend("<div class='pageloading-mask'><div></div></div>")
        };

        var removeOverlay = function (elt) {
            dom.body.$.find("div.pageloading-mask").remove();
        };

        return function () {
            dom.onDocument(events.zone.refresh, function () {
//                createOverlay(this);
            });
            dom.onDocument(events.zone.didUpdate, function () {
                removeOverlay(this);
            });
            dom.onDocument(events.form.prepareForSubmit, function () {
                createOverlay(this);
            });
        };
    });
}).call(this);

