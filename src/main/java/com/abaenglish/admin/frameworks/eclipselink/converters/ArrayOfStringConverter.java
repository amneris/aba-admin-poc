package com.abaenglish.admin.frameworks.eclipselink.converters;


import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;
//import org.postgresql.jdbc4.Jdbc4Array;
import org.postgresql.util.PGobject;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ArrayOfStringConverter implements Converter {

	@Override
	public Object convertObjectValueToDataValue(Object obj, Session session) {
		if (obj == null) {
			return null;
		} else if (obj instanceof Set<?>) {
			PGobject arrayObject = new PGobject();
			arrayObject.setType("_varchar");
			String data = "{" + ConverterUtils.collectionToCommaDelimitedString((Set<?>) obj) + "}";
			try {
				arrayObject.setValue(data);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return arrayObject;
		}
		return null;
	}

	@Override
	public Object convertDataValueToObjectValue(Object dataValue, Session session) {
		if (dataValue == null) {
			return null;
		} else {
			Set<String> collection = new HashSet<String>();
			if (dataValue instanceof PGobject) {
				PGobject p = (PGobject) dataValue;
				String data = p.getValue().replaceAll("[{}]", "");
				collection.addAll(ConverterUtils.commaDelimitedListToSet(data));
			} else if (dataValue instanceof String[]) {
				// automatically PgObject same Long[]
				Collections.addAll(collection, (String[]) dataValue);
/*
			} else if (dataValue instanceof Jdbc4Array) {
				try {
					Collections.addAll(collection, (String[]) ((Jdbc4Array) dataValue).getArray());
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
*/
			} else {
				throw new RuntimeException("can't convert PG data value to Java object value");
			}
			return collection;
		}
	}

	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		final DatabaseField field = mapping.getField();
		field.setSqlType(java.sql.Types.OTHER);
	}

	@Override
	public boolean isMutable() {
		return true;
	}
}
