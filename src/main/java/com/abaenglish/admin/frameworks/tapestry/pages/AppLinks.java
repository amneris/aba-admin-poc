package com.abaenglish.admin.frameworks.tapestry.pages;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Property;

import java.util.UUID;

public class AppLinks {

	@Property
	private UUID rewardId;

	@OnEvent(EventConstants.ACTIVATE)
	void activate(UUID rewardID) {
		this.rewardId = rewardID;
	}

}
