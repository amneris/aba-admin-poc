package com.abaenglish.admin.frameworks.eclipselink.converters;

import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectCollectionMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;

import java.util.Map;

public class HStoreConverter implements Converter {

	@Override
	public Object convertObjectValueToDataValue(Object objectValue, Session session) {
		return objectValue;
	}

	@Override
	public Object convertDataValueToObjectValue(Object dataValue, Session session) {
		return dataValue;
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		final DatabaseField field;
		if (mapping instanceof DirectCollectionMapping) {
			// handle @ElementCollection...
			field = ((DirectCollectionMapping) mapping).getDirectField();
		} else {
			field = mapping.getField();
		}

		field.setSqlType(java.sql.Types.OTHER);
		field.setTypeName(Map.class.getCanonicalName());
		field.setColumnDefinition("hstore");
	}
}