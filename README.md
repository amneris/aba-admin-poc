# ABA English Admin (very very rough draft)

The ABAEnglish Admin backoffice system is implemented in [Tapestry5](https://tapestry.apache.org/) using [Tynamo](http://tynamo.org/)

### Dependencies
+ [java 1.7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
+ [maven 3.2.5](https://maven.apache.org/)

### Read first ###

* http://tynamo.org/Overview
* http://tynamo.org/Quick+start

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

* Setup

		$ git clone --progress git@bitbucket.org:abaenglish/aba-admin.git
		$ cd aba-admin
		$ mvn jetty:run-forked

* Configuration
* Dependencies
* Database configuration
* How to run tests

Procfile
> web: java -Dtapestry.disable-default-modules=true $JAVA_OPTS -jar target/dependency/jetty-runner.jar --port $PORT target/*.war


