package com.abaenglish.admin.frameworks.tapestry.services;

import com.abaenglish.admin.frameworks.tapestry.Constants;
import com.abaenglish.admin.frameworks.tapestry.modules.RestApiModule;
import com.abaenglish.admin.frameworks.tapestry.modules.SecurityModule;
import com.abaenglish.admin.frameworks.tapestry.validators.JsonValidator;
import com.abaenglish.admin.frameworks.tynamo.ElasticSearchService;
import com.abaenglish.admin.frameworks.tynamo.GenericJPAService;
import org.apache.tapestry5.*;
import org.apache.tapestry5.beaneditor.DataTypeConstants;
import org.apache.tapestry5.beanvalidator.modules.BeanValidatorModule;
import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.*;
import org.apache.tapestry5.ioc.services.*;
import org.apache.tapestry5.jpa.EntityManagerSource;
import org.apache.tapestry5.jpa.JpaEntityPackageManager;
import org.apache.tapestry5.jpa.PersistenceUnitConfigurer;
import org.apache.tapestry5.jpa.TapestryPersistenceUnitInfo;
import org.apache.tapestry5.jpa.modules.JpaModule;
import org.apache.tapestry5.services.*;
import org.apache.tapestry5.upload.modules.UploadModule;
import org.apache.tapestry5.upload.services.UploadSymbols;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.tynamo.builder.Builder;
import org.tynamo.builder.BuilderDirector;
import org.tynamo.ckeditor.CKEditorModule;
import org.tynamo.model.jpa.TynamoJpaSymbols;
import org.tynamo.model.jpa.services.TynamoJpaModule;
import org.tynamo.routing.services.RoutingModule;
import org.tynamo.services.TynamoCoreModule;

import javax.persistence.spi.PersistenceUnitTransactionType;
import java.util.Date;
import java.util.UUID;

/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
@ImportModule({
		JpaModule.class,
		TynamoCoreModule.class,
		TynamoJpaModule.class,
		SecurityModule.class,
		RestApiModule.class,
		RoutingModule.class,
		BeanValidatorModule.class,
		CKEditorModule.class,
		UploadModule.class
//		RoutingHacksModule.class
})
public class AppModule
{
	public static void bind(ServiceBinder binder) {
		// binder.bind(MyServiceInterface.class, MyServiceImpl.class);

		// Make bind() calls on the binder object to define most IoC services.
		// Use service builder methods (example below) when the implementation
		// is provided inline, or requires more initialization than simply
		// invoking the constructor.
		binder.bind(GenericJPAService.class);
		binder.bind(ElasticSearchService.class);
	}

	@Contribute(SymbolProvider.class)
	@ApplicationDefaults
	public static void provideSymbols(MappedConfiguration<String, Object> configuration)
	{
		// Contributions to ApplicationDefaults will override any contributions to
		// FactoryDefaults (with the same key). Here we're restricting the supported
		// locales to just "en" (English). As you add localised message catalogs and other assets,
		// you can extend this list of locales (it's a comma separated series of locale names;
		// the first locale name is the default when there's no reasonable match).
		configuration.add(SymbolConstants.SUPPORTED_LOCALES, "en, es");

		configuration.add(SymbolConstants.JAVASCRIPT_INFRASTRUCTURE_PROVIDER, "jquery");
//		configuration.add(SymbolConstants.BOOTSTRAP_ROOT, "context:mybootstrap");
		configuration.add(SymbolConstants.MINIFICATION_ENABLED, true);

		// Set filesize limit to 2 MB
		configuration.add(UploadSymbols.REQUESTSIZE_MAX, "2048000");
		configuration.add(UploadSymbols.FILESIZE_MAX, "2048000");

		/**
		 * HMAC_PASSPHRASE is used to configure hash-based message authentication of Tapestry data stored in forms, or
		 * in the URL. Your application is less secure, and more vulnerable to denial-of-service attacks, when this
		 * symbol is not configured.
		 *
		 * If you need help getting your random string use this link:
		 *
		 * http://www.random.org/strings/?num=1&len=16&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new
		 *
		 */
		configuration.add(SymbolConstants.HMAC_PASSPHRASE, "DUgdKyC13PnWql4f");
		configuration.add(SymbolConstants.ENABLE_PAGELOADING_MASK, false);

		configuration.add(TynamoJpaSymbols.PERSISTENCEUNIT, Constants.PERSISTENCE_UNIT);
	}

	@Contribute(ComponentClassResolver.class)
	public static void addLibrary(Configuration<LibraryMapping> configuration)
	{
		configuration.add(new LibraryMapping("aba", "com.abaenglish.admin.frameworks.tapestry"));
	}

	@Contribute(SymbolProvider.class)
	@FactoryDefaults
	public static void provideFactoryDefaults(MappedConfiguration<String, Object> configuration)
	{
	}

	@Contribute(SymbolProvider.class)
	@FactoryDefaults
	public static void overrideFactorySymbols(MappedConfiguration<String, Object> configuration)
	{
		// The application version is incorprated into URLs for most assets. Web
		// browsers will cache assets because of the far future expires header.
		// If existing assets change (or if the Tapestry version changes) you
		// should also change this number, to force the browser to download new
		// versions. This overrides Tapesty's default (a random hexadecimal
		// number), but may be further overriden by DevelopmentModule or QaModule
		// by adding the same key in the contributeApplicationDefaults method.
		configuration.override(SymbolConstants.APPLICATION_VERSION, "1.0-SNAPSHOT");
	}

	/**
	 * Contribution to the BeanBlockSource service to tell the BeanEditForm component about the editors.
	 */
	@Contribute(BeanBlockSource.class)
	public static void addCustomBlocks(Configuration<BeanBlockContribution> configuration)
	{
		configuration.add(new DisplayBlockContribution("boolean", "blocks/DisplayBlocks", "check"));
		configuration.add(new DisplayBlockContribution("single-valued-association", "blocks/DisplayBlocks", "showPageLink"));
		configuration.add(new DisplayBlockContribution("many-valued-association", "blocks/DisplayBlocks", "showPageLinks"));
//		configuration.add(new DisplayBlockContribution("composition", "blocks/DisplayBlocks", "showPageLinks"));

		configuration.add(new DisplayBlockContribution("collection_link", "blocks/DisplayBlocks", "collection_link"));
		configuration.add(new EditBlockContribution("collection_link", "blocks/EditBlocks", "ignored"));

		configuration.add(new DisplayBlockContribution("hstore", "blocks/DisplayBlocks", "hstore"));
		configuration.add(new EditBlockContribution("hstore", "blocks/EditBlocks", "hstore"));

//		configuration.add(new DisplayBlockContribution("json", "blocks/DisplayBlocks", "hstore"));
		configuration.add(new EditBlockContribution("json", "blocks/EditBlocks", "json"));

		configuration.add(new DisplayBlockContribution("array", "blocks/DisplayBlocks", "array"));
		configuration.add(new EditBlockContribution("array", "blocks/EditBlocks", "array"));

		configuration.add(new DisplayBlockContribution("images", "blocks/DisplayBlocks", "images"));
		configuration.add(new EditBlockContribution("images", "blocks/EditBlocks", "array"));

		configuration.add(new DisplayBlockContribution("image", "blocks/DisplayBlocks", "image"));
		configuration.add(new EditBlockContribution("image", "PropertyEditBlocks", "text"));

		configuration.add(new DisplayBlockContribution("link", "blocks/DisplayBlocks", "link"));
		configuration.add(new EditBlockContribution("link", "PropertyEditBlocks", "text"));

		configuration.add(new DisplayBlockContribution("iframe", "blocks/DisplayBlocks", "iframe"));
		configuration.add(new EditBlockContribution("iframe", "PropertyEditBlocks", "text"));

	}

	@Contribute(BeanBlockOverrideSource.class)
	public static void overrideDefaultBlocks(Configuration<BeanBlockContribution> configuration) {
		configuration.add(new EditBlockContribution(DataTypeConstants.DATE, "blocks/EditBlocks", "date"));
		configuration.add(new EditBlockContribution("formatted-date", "blocks/EditBlocks", "date"));
	}

	/**
	 * Contributes Builders to the BuilderDirector's builders map.
	 * Check GOF's <a href="http://en.wikipedia.org/wiki/Builder_pattern">Builder pattern</a>
	 */
	@Contribute(BuilderDirector.class)
	public static void addBuilders(MappedConfiguration<Class, Builder> configuration)
	{
//		configuration.add(org.tynamo.examples.recipe.model.Recipe.class, new RecipeBuilder());
	}

	@Contribute(TypeCoercer.class)
	public static void uuidTypeCoercer(final Configuration<CoercionTuple> configuration) {
		configuration.add(CoercionTuple.create(UUID.class, String.class, new Coercion<UUID, String>() {
			@Override
			public String coerce(UUID uuid) {
				return uuid.toString();
			}
		}));
		configuration.add(CoercionTuple.create(String.class, UUID.class, new Coercion<String, UUID>() {
			@Override
			public UUID coerce(String s) {
				return UUID.fromString(s);
			}
		}));
	}

	@Contribute(TranslatorSource.class)
	public static void contributeUUIDTranslator(MappedConfiguration<Class, Translator> configuration) {
		configuration.add(UUID.class, new Translator<UUID>() {
			@Override
			public String getName() {
				return "uuid";
			}

			@Override
			public String toClient(UUID uuid) {
				return uuid.toString();
			}

			@Override
			public Class<UUID> getType() {
				return UUID.class;
			}

			@Override
			public String getMessageKey() {
				return "uuid";
			}

			@Override
			public UUID parseClient(Field field, String clientValue, String message) throws ValidationException {
				try {
					return UUID.fromString(clientValue);
				} catch (Exception e) {
					throw new ValidationException(message);
				}
			}

			@Override
			public void render(Field field, String s, MarkupWriter markupWriter, FormSupport formSupport) {

			}
		});
	}

	@Primary
	@Contribute(DataTypeAnalyzer.class)
	public static void contributeUUIDAnalyzer(OrderedConfiguration<DataTypeAnalyzer> configuration)
	{
		configuration.add("UUID", new DataTypeAnalyzer() {
			@Override
			public String identifyDataType(PropertyAdapter adapter) {
				return adapter.getType().isAssignableFrom(UUID.class) ? "text" : null;
			}
		});
	}

	@Contribute(FieldValidatorSource.class)
	public static void addValidators(MappedConfiguration<String, Validator> configuration)
	{
		configuration.addInstance("json", JsonValidator.class);
		configuration.addInstance("fail", JsonValidator.class);
	}

	@SuppressWarnings("unchecked")
	public static void contributeTypeCoercer(final Configuration<CoercionTuple> configuration)
	{
		configuration.add(CoercionTuple.create(Date.class, String.class, new Coercion<Date, String>() {
			@Override
			public String coerce(Date input) {
				return Long.toString(input.getTime());
			}
		}));
		configuration.add(CoercionTuple.create(String.class, Date.class, new Coercion<String, Date>() {
			@Override
			public Date coerce(String input) {
				return new Date(Long.valueOf(input));
			}
		}));
	}
	/*

	// This will override the bundled bootstrap version and will compile it at runtime
	@Contribute(JavaScriptStack.class)
	@Core
	public static void overrideBootstrapCSS(OrderedConfiguration<StackExtension> configuration)
	{
		configuration.override("bootstrap.css",
				new StackExtension(StackExtensionType.STYLESHEET, "context:mybootstrap/css/bootstrap.css"), "before:tapestry.css");
	}
*/

	@Contribute(JpaEntityPackageManager.class)
	public static void addPackagesToScan(Configuration<String> configuration) {
		configuration.add("com.abaenglish.admin.domain.model");
	}

	@Contribute(EntityManagerSource.class)
	public static void configurePersistenceUnits(MappedConfiguration<String, PersistenceUnitConfigurer> cfg,
	                                             @Inject @Symbol(com.abaenglish.admin.frameworks.tapestry.SymbolConstants.DB_USERNAME) final String user,
	                                             @Inject @Symbol(com.abaenglish.admin.frameworks.tapestry.SymbolConstants.DB_PASSWORD) final String password,
	                                             @Inject @Symbol(com.abaenglish.admin.frameworks.tapestry.SymbolConstants.DB_URL) final String url) {
		cfg.add(Constants.PERSISTENCE_UNIT, new PersistenceUnitConfigurer() {
			@Override
			public void configure(TapestryPersistenceUnitInfo unitInfo) {
				unitInfo.transactionType(PersistenceUnitTransactionType.RESOURCE_LOCAL)
						.persistenceProviderClassName("org.eclipse.persistence.jpa.PersistenceProvider")
						.excludeUnlistedClasses(false)
						.addProperty("javax.persistence.jdbc.user", user)
						.addProperty("javax.persistence.jdbc.password", password)
						.addProperty("javax.persistence.jdbc.driver", "org.postgresql.Driver")
						.addProperty("javax.persistence.jdbc.url", url)
						.addProperty(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.CREATE_OR_EXTEND)
						.addProperty("eclipselink.jpa.uppercase-column-names", "true")
						.addProperty("eclipselink.cache.shared.default", "false")
						.addProperty("eclipselink.logging.level", "FINE")

						.addProperty("eclipselink.connection-pool.default.initial", "1")
						.addProperty("eclipselink.connection-pool.default.min", "1")
						.addProperty("eclipselink.connection-pool.default.max", "4")
						.addProperty("eclipselink.jdbc.read-connections.min", "1")
						.addProperty("eclipselink.jdbc.write-connections.min", "1")
						.addProperty("eclipselink.jdbc.read-connections.max", "4")
						.addProperty("eclipselink.jdbc.write-connections.max", "4");
			}
		});
	}
}
