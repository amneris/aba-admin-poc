package com.abaenglish.admin.frameworks.tapestry.pages;


import com.abaenglish.admin.domain.model.Student;
import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.FieldFocusPriority;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.annotations.*;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.jpa.annotations.CommitAfter;
import org.apache.tapestry5.services.ContextValueEncoder;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.apache.tapestry5.services.ajax.JavaScriptCallback;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.tynamo.descriptor.annotation.beaneditor.BeanModel;
import org.tynamo.descriptor.annotation.beaneditor.BeanModels;
import org.tynamo.routing.annotations.At;
import org.tynamo.services.DescriptorService;
import org.tynamo.services.PersistenceService;
import org.tynamo.util.TynamoMessages;
import org.tynamo.util.Utils;

import javax.validation.ConstraintViolation;
import javax.validation.ValidatorFactory;

/**
 * Page for editing and updating objects.
 *
 * @note:
 * When extending this page for customization purposes, it's better to copy & paste code than trying to use inheritance.
 *
 */
@At("/{0}/{1}/edit")
public class Edit
{
	@Inject
	private ContextValueEncoder contextValueEncoder;

	@Inject
	private Messages messages;

	@Inject
	private PersistenceService persistenceService;

	@Inject
	private PageRenderLinkSource pageRenderLinkSource;

	@Inject
	private AjaxResponseRenderer ajaxResponseRenderer;

	@Inject
	private ValidatorFactory validatorFactory;

	@Inject
	private JavaScriptSupport javascriptSupport;

	@InjectComponent
	private Zone formZone;

	@InjectComponent
	private Form form;

	@Property(write = false)
	private Class beanType;

	@Property
	private Object bean;

	private boolean continueEditing;

	@OnEvent(EventConstants.ACTIVATE)
	Object activate(Class clazz, String id)
	{

		if (clazz == null) return Utils.new404(messages);

		this.bean = contextValueEncoder.toValue(clazz, id);
		this.beanType = clazz;

		if (bean == null) return Utils.new404(messages);

		return null;
	}

	@CleanupRender
	void cleanup()
	{
		bean = null;
		beanType = null;
	}

	@OnEvent(EventConstants.VALIDATE)
	void validate() {
		for (ConstraintViolation violation : validatorFactory.getValidator().validate(bean)) {
			form.recordError(violation.getMessage());
		}
	}

	/**
	 * This tells Tapestry to put type & id into the URL, making it bookmarkable.
	 */
	@OnEvent(EventConstants.PASSIVATE)
	Object[] passivate()
	{
		return new Object[]{beanType, bean};
	}

	@Log @OnEvent(value = "return")
	void onSaveAndReturn() {
		this.continueEditing = false;
	}

	@Log @OnEvent(value = "stay")
	void onSaveAndContinue() {
		this.continueEditing = true;
	}

	@Log
	@CommitAfter
	@OnEvent(EventConstants.SUCCESS)
	Link success()
	{
		persistenceService.save(bean);
		return !continueEditing ? back() : null;
	}

	@OnEvent(EventConstants.FAILURE)
	void failure()
	{
		ajaxResponseRenderer
				.addRender(formZone)
				.addCallback(new JavaScriptCallback() {
					@Override
					public void run(JavaScriptSupport javascriptSupport) {
						javascriptSupport.autofocus(FieldFocusPriority.OVERRIDE, "");
						javascriptSupport.require("t5/core/pageinit")
								.invoke("evalJavaScript")
								.with("$('html, body').animate({ scrollTop: $('#formZone').offset().top - 65 }, 'slow');");
					}
				});
	}

	@OnEvent("cancel")
	Link back()
	{
		return pageRenderLinkSource.createPageRenderLinkWithContext(Show.class, beanType, bean);
	}

	public String getListAllLinkMessage()
	{
		return TynamoMessages.listAll(messages, beanType);
	}

	public String getTitle()
	{
		return TynamoMessages.edit(messages, beanType);
	}

	@Inject
	private DescriptorService descriptorService;

	public boolean isWeakEntity()
	{
		return descriptorService.getClassDescriptor(beanType).isNonVisual();
	}

}
