package com.abaenglish.admin.frameworks.tapestry.pages.blocks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.apache.tapestry5.FieldTranslator;
import org.apache.tapestry5.FieldValidator;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Environmental;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.DateField;
import org.apache.tapestry5.corelib.components.TextArea;
import org.apache.tapestry5.services.PropertyEditContext;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class EditBlocks {

	@Environmental
	@Property(write = false)
	private PropertyEditContext propertyEditContext;

	@Component(parameters = {"value=propertyEditContext.propertyValue", "label=prop:propertyEditContext.label",
			"translate=prop:hstoreFieldTranslator", "validate=prop:hstoreFieldValidator",
			"clientId=prop:propertyEditContext.propertyId", "annotationProvider=propertyEditContext"})
	private TextArea hstoreField;

	@SuppressWarnings("unused")
	@Component(parameters = {"value=propertyEditContext.propertyValue", "label=prop:propertyEditContext.label",
			"clientId=prop:propertyEditContext.propertyid", "validate=prop:dateFieldValidator",
			"ensureClientIdUnique=true", "format=prop:dateFormat"})
	private DateField dateField;

	@Component(parameters = {"value=propertyEditContext.propertyValue", "label=prop:propertyEditContext.label",
			"translate=prop:jsonFieldTranslator", "validate=prop:jsonFieldValidator",
			"clientId=prop:propertyEditContext.propertyId", "annotationProvider=propertyEditContext"})
	private TextArea jsonField;

	@Component(parameters = {"value=propertyEditContext.propertyValue", "label=prop:propertyEditContext.label",
			"translate=prop:arrayFieldTranslator", "validate=prop:arrayFieldValidator",
			"clientId=prop:propertyEditContext.propertyId", "annotationProvider=propertyEditContext"})
	private TextArea arrayField;

	public FieldTranslator getHstoreFieldTranslator() {
		return new FieldTranslator() {
			@Override
			public Class getType() {
				return Map.class;
			}

			@Override
			public Object parse(String json) throws ValidationException {
				Gson gson = getGson();
				Type type = new TypeToken<Map<String, String>>(){}.getType();
				try {
					return gson.fromJson(json, type);
				} catch (JsonSyntaxException e) {
					throw new ValidationException(e.getMessage());
				}
			}

			@Override
			public String toClient(Object o) {
				Gson gson = getGson();
				return gson.toJson((Map) o);
			}

			@Override
			public void render(MarkupWriter markupWriter) {

			}
		};
	}

	public FieldTranslator getJsonFieldTranslator() {
		return propertyEditContext.getTranslator(jsonField);
	}

	public FieldTranslator getArrayFieldTranslator() {
		return new FieldTranslator() {
			@Override
			public Class getType() {
				return List.class;
			}

			@Override
			public Object parse(String json) throws ValidationException {
				Gson gson = getGson();
				Type type = new TypeToken<List<String>>(){}.getType();
				try {
					return gson.fromJson(json, type);
				} catch (JsonSyntaxException e) {
					throw new ValidationException(e.getMessage());
				}
			}

			@Override
			public String toClient(Object o) {
				Gson gson = getGson();
				return gson.toJson((List) o);
			}

			@Override
			public void render(MarkupWriter markupWriter) {

			}
		};
	}

	private Gson getGson() {
		return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
	}


	public FieldValidator getHstoreFieldValidator() {
		return propertyEditContext.getValidator(hstoreField);
	}

	public FieldValidator getJsonFieldValidator() {
		return new FieldValidator() {
			@Override
			public void validate(Object value) throws ValidationException {
				Gson gson = getGson();
				Type type = new TypeToken<Map<String, String>>() {
				}.getType();
				try {
					gson.fromJson((String) value, type);
				} catch (JsonSyntaxException e) {
					throw new ValidationException(e.getMessage());
				}
			}

			@Override
			public void render(MarkupWriter markupWriter) {

			}

			@Override
			public boolean isRequired() {
				return false;
			}
		};
	}

	public FieldValidator getDateFieldValidator() {
		return propertyEditContext.getValidator(dateField);
	}

	public FieldValidator getArrayFieldValidator() {
		return propertyEditContext.getValidator(arrayField);
	}

	public DateFormat getDateFormat() {
/*
		if (!isBeanModelAdvisorMixinUsed()) return defaultDateFormat;

		String format = getPropertyDescriptor().getFormat();
		return format != null ? new SimpleDateFormat(format) : defaultDateFormat;
*/
		return new SimpleDateFormat("yyyy-MM-dd HH:mm");
	}


}
