package com.abaenglish.admin.frameworks.tynamo;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.pac4j.oauth.profile.google2.Google2Profile;
import org.slf4j.Logger;
import org.tynamo.security.federatedaccounts.services.FederatedAccountService;

import java.util.Date;

public class FederatedAccountServiceImpl implements FederatedAccountService {

	public static final String SUFFIX = "@abaenglish.com";
	public final Logger logger;

	public FederatedAccountServiceImpl(Logger logger) {
		this.logger = logger;
	}

	@Override
	public AuthenticationInfo federate(String realmName, Object remotePrincipal, AuthenticationToken authenticationToken, Object remoteUser) {

		if (remoteUser instanceof Google2Profile) {
			Google2Profile profile = (Google2Profile) remoteUser;
			if (profile.getEmail().endsWith(SUFFIX)) {
				String username = profile.getEmail().substring(0, profile.getEmail().length() - SUFFIX.length());
				return newAuthenticationInfo(realmName, authenticationToken, username);
			}
			throw new IncorrectCredentialsException("Account [" + profile.getEmail() + "] is not allowed to connect to this service.");
		}

		throw new IncorrectCredentialsException("Account [" + remotePrincipal + "] is not allowed to connect to this service.");
	}

	private AuthenticationInfo newAuthenticationInfo(String realmName, AuthenticationToken authenticationToken, String username) {
		SimplePrincipalCollection principalCollection = new SimplePrincipalCollection(username, realmName);
		logger.info("ACCOUNT LOGIN: {} - {}", username, new Date());
		return new SimpleAuthenticationInfo(principalCollection, authenticationToken.getCredentials());
	}
}
