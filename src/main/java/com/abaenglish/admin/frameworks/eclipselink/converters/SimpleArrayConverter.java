package com.abaenglish.admin.frameworks.eclipselink.converters;

import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectCollectionMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;
//import org.postgresql.jdbc4.Jdbc4Array;
import org.postgresql.util.PGobject;

import java.sql.SQLException;

public class SimpleArrayConverter implements Converter {

	@Override
	public Object convertObjectValueToDataValue(Object objectValue, Session session) {
		if (objectValue == null) {
			return null;
		} else if (objectValue instanceof String) {
			PGobject arrayObject = new PGobject();
			arrayObject.setType("_text");
			try {
				arrayObject.setValue((String) objectValue);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return arrayObject;
		}
		return null;
	}

	@Override
	public Object convertDataValueToObjectValue(Object dataValue, Session session) {
		if (dataValue == null) {
			return null;
		} else {
			if (dataValue instanceof PGobject) {
				PGobject p = (PGobject) dataValue;
				return p.getValue();
			} else if (dataValue instanceof String[]) {
				return "{" + ConverterUtils.arrayToCurliedDelimitedString((String[]) dataValue, ",") + "}";
/*
			} else if (dataValue instanceof Jdbc4Array) {
				try {
					return "{" + ConverterUtils.arrayToCurliedDelimitedString((String[]) ((Jdbc4Array) dataValue).getArray(), ",") + "}";
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
*/
			} else {
				throw new RuntimeException("can't convert PG data value to Java object value");
			}
		}
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		final DatabaseField field;

		if (mapping instanceof DirectCollectionMapping) {
			// handle @ElementCollection...
			field = ((DirectCollectionMapping) mapping).getDirectField();
		} else {
			field = mapping.getField();
		}

		field.setSqlType(java.sql.Types.OTHER);
	}
}