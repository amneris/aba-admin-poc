package com.abaenglish.admin.frameworks.eclipselink.converters;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;
import java.util.Map;


@Converter(autoApply = true)
public class MapToStringConveter implements AttributeConverter<Map<String, String>, Object> {

	@Override
	public String convertToDatabaseColumn(Map<String, String> attribute) {
		if (attribute == null || attribute.isEmpty()) {
			return "";
		}
		try {
			return org.postgresql.util.HStoreConverter.toString(attribute);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	@Override
	public Map<String, String> convertToEntityAttribute(Object dbData) {
		// EclipseLink Bug or Feature? Hstore will be converted to HashMap, not String...,
		// so the target type should be Object
		if (dbData instanceof String) {
			String str = (String) dbData;
			if (str.trim().length() == 0) {
				return new HashMap<String, String>();
			}
			return org.postgresql.util.HStoreConverter.fromString((String) dbData);

		} else {
			return (Map<String, String>) dbData;
		}
	}
}