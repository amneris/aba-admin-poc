package com.abaenglish.admin.frameworks.eclipselink.converters;

import org.apache.tapestry5.json.JSONObject;
import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectCollectionMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;

import java.util.Map;

public class SimpleHStoreConverter implements Converter {

	@Override
	public Object convertObjectValueToDataValue(Object objectValue, Session session) {
		if (objectValue != null) {
			JSONObject object = new JSONObject((String) objectValue);
			return object.toMap();
		}
		return null;
	}

	@Override
	public Object convertDataValueToObjectValue(Object dataValue, Session session) {
		if (dataValue != null && dataValue instanceof Map) {
			JSONObject object = new JSONObject();
			object.putAll((Map) dataValue);
			return object.toString();
		}
		return dataValue;
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		final DatabaseField field;

		if (mapping instanceof DirectCollectionMapping) {
			// handle @ElementCollection...
			field = ((DirectCollectionMapping) mapping).getDirectField();
		} else {
			field = mapping.getField();
		}

		field.setSqlType(java.sql.Types.OTHER);
		field.setTypeName(Map.class.getCanonicalName());
		field.setColumnDefinition("hstore");
	}
}