package com.abaenglish.admin.frameworks.tynamo;

/**
 * This is mostly a copy of ElasticSearchIndexMaintainer.
 * There is no access to ElasticSearchIndexMaintainer thru the Registry, and even though I could create a new instance
 * of it, most of the methods needed for recreating the index (Issue #774) are private or protected.
 *
 * check http://jira.codehaus.org/browse/TYNAMO-233
 *
 */
public class ElasticSearchServiceImpl implements ElasticSearchService {

	@Override
	public void rebuildIndex(Class entityType) {

	}

	/*
	private final DescriptorService descriptorService;
	private final GenericJPAService jpa;
	private final Logger logger;
	private final MapperFactory mapperFactory;
	private final Node node;

	public ElasticSearchServiceImpl(DescriptorService descriptorService, GenericJPAService jpa, Logger logger, MapperFactory mapperFactory, Node node) {
		this.descriptorService = descriptorService;
		this.jpa = jpa;
		this.logger = logger;
		this.mapperFactory = mapperFactory;
		this.node = node;
	}

	@Override
	public void rebuildIndex(Class entityType) {

		TynamoClassDescriptor descriptor = descriptorService.getClassDescriptor(entityType);
		if (!descriptor.supportsExtension(ElasticSearchExtension.class)) return;

		ElasticSearchExtension extension = descriptor.getExtension(ElasticSearchExtension.class);

		Client client = node.client();
		client.admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet();

		deleteIndex(client, extension);
		createIndex(client, extension);
		indexEntities(client, extension, jpa.findAll(entityType));
		refreshIndex(client, extension);

	}

	private void refreshIndex(Client client, ElasticSearchExtension extension) {
		client.admin().indices().refresh(new RefreshRequest(extension.getIndexName())).actionGet();
	}

	private void deleteIndex(Client client, ElasticSearchExtension extension) {
		try {
			DeleteIndexResponse delete = client.admin().indices().delete(new DeleteIndexRequest(extension.getIndexName())).actionGet();
			if (!delete.getAcknowledged()) {
				logger.error("Index wasn't deleted");
			}
		} catch (IndexMissingException e) {
			// ignore it.
		}
	}

	private void createIndex(Client client, ElasticSearchExtension descriptor) {
		String indexName = descriptor.getIndexName();
		try {
			logger.debug("Starting Elastic Search Index {}", indexName);
			CreateIndexResponse response = client.admin().indices().create(new CreateIndexRequest(indexName)).actionGet();
			logger.debug("Response: {}", response);

		} catch (IndexAlreadyExistsException iaee) {
			logger.debug("Index already exists: {}", indexName);
			return;
		} catch (Throwable t) {
			logger.warn(ExceptionUtils.getStackTrace(t));
			return;
		}
		createType(client, descriptor);
	}

	private void createType(Client client, ElasticSearchExtension descriptor) {
		String indexName = descriptor.getIndexName();
		String typeName = descriptor.getTypeName();

		try {
			logger.debug("Create Elastic Search Type {}/{}", indexName, typeName);
			PutMappingRequest request = Requests.putMappingRequest(indexName).type(typeName);
			XContentBuilder contentBuilder = XContentFactory.jsonBuilder().prettyPrint();
			descriptor.addMapping(contentBuilder, mapperFactory);
			logger.debug("Type mapping: \n {}", contentBuilder.string());
			request.source(contentBuilder);
			PutMappingResponse response = client.admin().indices().putMapping(request).actionGet();
			logger.debug("Response: {}", response);

		} catch (IndexAlreadyExistsException iaee) {
			logger.debug("Index already exists: {}", indexName);

		} catch (Throwable t) {
			logger.warn(ExceptionUtils.getStackTrace(t));
		}
	}

	private boolean indexEntities(Client client, ElasticSearchExtension descriptorExtension, List entities) {
		try {
			for (Object entity : entities) {
				indexEntity(client, descriptorExtension, entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void indexEntity(Client client, ElasticSearchExtension descriptor, Object model) throws Exception {
		logger.debug("Index Model: {}", model);
		XContentBuilder contentBuilder = null;
		// Index Model
		try {
			contentBuilder = XContentFactory.jsonBuilder().prettyPrint();
			descriptor.addModel(model, contentBuilder, mapperFactory);
			logger.debug("Index json: {}", contentBuilder.string());
			IndexResponse response = client
					.prepareIndex(descriptor.getIndexName(), descriptor.getTypeName(), descriptor.getDocumentId(model))
					.setSource(contentBuilder).execute()
					.actionGet();

			logger.debug("Index Response: {}", response);

		} finally {
			if (contentBuilder != null) contentBuilder.close();
		}
	}
*/
}
