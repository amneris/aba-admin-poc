package com.abaenglish.admin.frameworks.tapestry.validators;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.apache.tapestry5.Field;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.ValidationException;
import org.apache.tapestry5.ioc.MessageFormatter;
import org.apache.tapestry5.services.FormSupport;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.apache.tapestry5.validator.AbstractValidator;

import java.lang.reflect.Type;
import java.util.Map;

public class JsonValidator extends AbstractValidator<Void, String> {


	public JsonValidator(JavaScriptSupport javaScriptSupport) {
		super(null, String.class, "invalid-json", javaScriptSupport);
	}

	@Override
	public void validate(Field field, Void aVoid, MessageFormatter messageFormatter, String value) throws ValidationException {
			Gson gson = new Gson();
			Type type = new TypeToken<Map<String, String>>(){}.getType();
			try {
				gson.fromJson(value, type);
			} catch (JsonSyntaxException e) {
				throw new ValidationException(e.getMessage());
			}
	}

	@Override
	public void render(Field field, Void aVoid, MessageFormatter messageFormatter, MarkupWriter markupWriter, FormSupport formSupport) {

	}
}
