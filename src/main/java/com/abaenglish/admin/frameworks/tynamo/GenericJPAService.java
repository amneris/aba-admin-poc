package com.abaenglish.admin.frameworks.tynamo;

import com.abaenglish.admin.frameworks.tapestry.CriteriaQueryModifier;
import org.apache.tapestry5.jpa.annotations.CommitAfter;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Set;

public interface GenericJPAService {

	/**
	 * Finds all T entities that match the given parameters. Supports two types of enhanced syntax that can be mixed together.
	 * <ol>
	 * <li>prop1.prop2: Makes joins by navigating the entity graph with dot syntax</li>
	 * <li>prop1?.prop2: Same as the above except the join is a LEFT join (XXX: NOT SUPPORTED YET)</li>
	 * </ol>
	 *
	 * @param entityClass    The class of the entity
	 * @param nameValuePairs pairs of property=value restrictions
	 * @return
	 */
	<T> List<T> findAll(Class<T> entityClass, Object... nameValuePairs);

	/**
	 * Finds all T entities that match the given parameters. Supports two types of enhanced syntax that can be mixed together.
	 * <ol>
	 * <li>prop1.prop2: Makes joins by navigating the entity graph with dot syntax</li>
	 * <li>prop1?.prop2: Same as the above except the join is a LEFT join (XXX: NOT SUPPORTED YET)</li>
	 * </ol>
	 *
	 * @param entityClass    The class of the entity
	 * @param nameValuePairs pairs of property=value restrictions
	 * @return
	 */
	<T> long findAllCount(Class<T> entityClass, Object... nameValuePairs);

	/**
	 * @param entityClass
	 * @param nameValuePairs
	 * @return
	 */
	<T> T find(Class<T> entityClass, Object... nameValuePairs);

	/**
	 * @param entityClass
	 * @param nameValuePairs
	 * @return
	 */
	<T> CriteriaQuery<T> criteriaQuery(Class<T> entityClass, Object... nameValuePairs);

	/**
	 * Saves or updates the entity, and commits after.
	 */
	@CommitAfter
	void save(Object entity);

	/**
	 * Deletes the entity, and commits after.
	 */
	@CommitAfter
	void delete(Object entity);

	<T> CriteriaQuery<T> createQuery(Class<T> entityClass, CriteriaQueryModifier<T> modifier, Object... nameValuePairs);

	<T> List<T> findAll(Class<T> entityClass, CriteriaQueryModifier<T> modifier, Object... nameValuePairs);

	<T> List<T> findIn(Class<T> beanType, Set ids);
}
