package com.abaenglish.admin.frameworks.tynamo;

import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.util.StringUtils;
import org.apache.shiro.web.util.WebUtils;
import org.tynamo.security.internal.services.LoginContextService;
import org.tynamo.security.shiro.authz.AuthorizationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * A Filter that can allow or deny access based on the host that sent the request.
 */
public class HostFilter extends AuthorizationFilter {

	public HostFilter(LoginContextService loginContextService) {
		super(loginContextService);
	}

	protected String toHostname(Object mappedValue) {
		String[] hosts = (String[]) mappedValue;
		if (hosts == null || hosts.length == 0) {
//			return getDefaultHostname();
			throw new ConfigurationException("HostFilter must be configured with a hostname.  You have " +
					"configured " + hosts.length + ": " + StringUtils.toString(hosts));

		}
		if (hosts.length > 1) {
			throw new ConfigurationException("HostFilter can only be configured with a single hostname.  You have " +
					"configured " + hosts.length + ": " + StringUtils.toString(hosts));
		}
		return expand(hosts[0]);
	}

	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
		String requiredHostname = toHostname(mappedValue);
		String requestHostname = request.getServerName();
		return requiredHostname.equals(requestHostname);
	}

	/**
	 * Redirects the request to the same exact incoming URL, but with the port listed in the filter's configuration.
	 *
	 * @param request     the incoming <code>ServletRequest</code>
	 * @param response    the outgoing <code>ServletResponse</code>
	 * @param mappedValue the config specified for the filter in the matching request's filter chain.
	 * @return {@code false} always to force a redirect.
	 */
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {

		int port = request.getServerPort();

		StringBuilder sb = new StringBuilder();
		sb.append(request.getScheme()).append("://");
		sb.append(toHostname(mappedValue));
		if (port != 80 && port != 443) {
			sb.append(":");
			sb.append(port);
		}
		if (request instanceof HttpServletRequest) {
			sb.append(WebUtils.toHttp(request).getRequestURI());
			String query = WebUtils.toHttp(request).getQueryString();
			if (query != null) {
				sb.append("?").append(query);
			}
		}

		WebUtils.issueRedirect(request, response, sb.toString());

		return false;
	}

	private String expand(final String hostname) {
		// This is common in some PaaS deployments, such as Heroku, where the port is passed in as
		// and environment variable.
		if (hostname.startsWith("$")) {
			String envHostName = System.getenv(hostname.substring(1));
			if (envHostName == null) throw new ConfigurationException("Make sure the $HOSTNAME environment variable is properly configured");
			return envHostName;
		}
		return hostname;
	}
}
