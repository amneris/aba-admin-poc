package com.abaenglish.admin.frameworks.tapestry.pages;

import com.abaenglish.admin.domain.model.Student;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.Cached;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.tynamo.routing.annotations.At;
import org.tynamo.services.PersistenceService;

/**
 * Start page of application aba-admin.
 */
@At("/")
public class Home
{
	@Property
	@Inject
	@Symbol(SymbolConstants.EXECUTION_MODE)
	private String executionMode;

	@Inject
	private PersistenceService persistenceService;

	@Cached
	public int getStudentCount() {
		return persistenceService.count(Student.class);
	}

	public Class getStudentClass() {
		return Student.class;
	}
}