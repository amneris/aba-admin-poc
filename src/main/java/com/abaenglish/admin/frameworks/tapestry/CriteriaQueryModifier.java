package com.abaenglish.admin.frameworks.tapestry;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public interface CriteriaQueryModifier<T> {
	void applyAdditionalConstraints(CriteriaBuilder cb, CriteriaQuery<?> q, Root<T> r);
}
