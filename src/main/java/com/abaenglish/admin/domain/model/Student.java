package com.abaenglish.admin.domain.model;

import com.abaenglish.admin.frameworks.eclipselink.converters.UUIDConverter;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.tynamo.model.elasticsearch.annotations.ElasticSearchField;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "students")
// @ElasticSearchable // ElasticSearch doesn't work on Heroku :(
public class Student {

	private UUID id;
	private String name;
	private String email;

	private String deviceId;
	private Date createdOn;
	private String facebookId;
	private String language;
	private String pushRegistrationId;
	private String gaid;

	private Date lastSeenOn;

	@Id
	@GeneratedValue(generator = "REWARD_ID_GEN")
	@org.eclipse.persistence.annotations.Converter(name = "uuidConverter", converterClass = UUIDConverter.class)
	@org.eclipse.persistence.annotations.Convert("uuidConverter")
	@ElasticSearchField
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}


	@Column(name = "device_id")
	@NotNull
	@ElasticSearchField
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Column(name = "created_on")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "facebook_id")
	@ElasticSearchField
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Column(name = "push_registration_id")
	public String getPushRegistrationId() {
		return pushRegistrationId;
	}

	public void setPushRegistrationId(String pushRegistrationId) {
		this.pushRegistrationId = pushRegistrationId;
	}

	@ElasticSearchField
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "google_aid")
	@ElasticSearchField
	public String getGaid() {
		return gaid;
	}

	public void setGaid(String gaid) {
		this.gaid = gaid;
	}

	@Email
	@ElasticSearchField
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "last_seen_on")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastSeenOn() {
		return lastSeenOn;
	}

	public void setLastSeenOn(Date lastSeenOn) {
		this.lastSeenOn = lastSeenOn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Student that = (Student) o;

		return id == null ? that.id == null : id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString() {
		return StringUtils.isNotEmpty(name) ? name : deviceId;
	}
}
