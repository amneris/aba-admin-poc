package com.abaenglish.admin.frameworks.eclipselink.converters;

import org.eclipse.persistence.internal.helper.DatabaseField;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;
import org.postgresql.util.PGobject;

import java.sql.SQLException;

public class JSONConverter implements Converter {

	@Override
	public Object convertObjectValueToDataValue(Object objectValue, Session session) {
		try {
			PGobject p = new PGobject();
			p.setType("json");
			p.setValue((String) objectValue);
			return p;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object convertDataValueToObjectValue(Object dataValue, Session session) {
		if (dataValue == null) {
			return null;
		} else if (dataValue instanceof PGobject) {
			PGobject p = (PGobject) dataValue;
			if (p.getType().equals("json")) return p.getValue();
		}
		return dataValue;
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public void initialize(DatabaseMapping mapping, Session session) {
		final DatabaseField field = mapping.getField();
		field.setSqlType(java.sql.Types.OTHER);
	}
}