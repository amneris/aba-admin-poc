package com.abaenglish.admin.frameworks.tapestry.modules;

import com.abaenglish.admin.frameworks.tynamo.HostFilter;
import com.abaenglish.admin.frameworks.tynamo.FederatedAccountServiceImpl;
import org.apache.shiro.realm.Realm;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.Configuration;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.ioc.annotations.Contribute;
import org.apache.tapestry5.ioc.annotations.ImportModule;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.ioc.services.ApplicationDefaults;
import org.apache.tapestry5.ioc.services.SymbolProvider;
import org.tynamo.security.SecuritySymbols;
import org.tynamo.security.federatedaccounts.pac4j.Pac4jFederatedAccountsModule;
import org.tynamo.security.federatedaccounts.pac4j.services.Pac4jFederatedRealm;
import org.tynamo.security.federatedaccounts.services.FederatedAccountService;
import org.tynamo.security.federatedaccounts.services.FederatedAccountsModule;
import org.tynamo.security.internal.services.LoginContextService;
import org.tynamo.security.services.SecurityFilterChainFactory;
import org.tynamo.security.services.impl.SecurityFilterChain;

@ImportModule({
		org.tynamo.security.services.SecurityModule.class,
		FederatedAccountsModule.class,
		Pac4jFederatedAccountsModule.class
})
public class SecurityModule {

	public static void bind(ServiceBinder binder) {
		binder.bind(FederatedAccountService.class, FederatedAccountServiceImpl.class);
	}

	@Contribute(SymbolProvider.class)
	@ApplicationDefaults
	public static void applicationDefaults(MappedConfiguration<String, Object> configuration) {

		// Tynamo's tapestry-security (Shiro) module configuration
		configuration.add(SecuritySymbols.LOGIN_URL, "/signin");
		configuration.add(SecuritySymbols.UNAUTHORIZED_URL, "/unauthorized");
		configuration.add(SecuritySymbols.SUCCESS_URL, "/home");

		/**
		 * Authorized redirect URIs
		 *
		 * http://localhost:8080/federated/pac4joauth/self/google2
		 */
		configuration.add(Pac4jFederatedRealm.GOOGLE_CLIENTID, "213668991404-5i20chitc01591ockc281t5k0mo8sdip.apps.googleusercontent.com");
		configuration.add(Pac4jFederatedRealm.GOOGLE_CLIENTSECRET, "Rbho3di2sQAwj_ikULQ0tORr");
	}

	public static void contributeWebSecurityManager(Configuration<Realm> configuration)
	{
//		configuration.add(new ExtendedPropertiesRealm("classpath:shiro-users.properties"));
	}


	public static void contributeSecurityConfiguration(Configuration<SecurityFilterChain> configuration,
	                                                   SecurityFilterChainFactory factory,
	                                                   LoginContextService loginContextService,
	                                                   @Inject @Symbol(SymbolConstants.HOSTNAME) String hostname)
	{
		HostFilter hostFilter = new HostFilter(loginContextService);

		configuration.add(factory.createChain("/assets/**").add(factory.anon()).build());
		configuration.add(factory.createChain("/modules.gz/**").add(factory.anon()).build());

		configuration.add(factory.createChain("/signin*/**").add(hostFilter, hostname).add(factory.anon()).build());
		configuration.add(factory.createChain("/federated*/**").add(hostFilter, hostname).add(factory.anon()).build());
		configuration.add(factory.createChain("/unauthorized*/**").add(hostFilter, hostname).add(factory.anon()).build());

//		configuration.add(factory.createChain("/**").add(factory.authc()).add(hostFilter, hostname).build());

/*
		configuration.add(factory.createChain("/edit/**").add(factory.perms(), "*:update").build());
		configuration.add(factory.createChain("/show/**").add(factory.perms(), "*:select").build());
		configuration.add(factory.createChain("/add/**").add(factory.perms(), "*:insert").build());
		configuration.add(factory.createChain("/list/**").add(factory.perms(), "*:select").build());
 */
	}

/*
	@Contribute(ServiceOverride.class)
	public void setupCustomRememberMeManager(MappedConfiguration<Class, Object> configuration) {
		CookieRememberMeManager rememberMeManager = new CookieRememberMeManager();
		// the default Shiro serializer produces obnoxiously long cookies
		rememberMeManager.setSerializer(new SimplePrincipalSerializer());
		rememberMeManager.getCookie().setDomain("adm.abaenglish.com");
		configuration.add(RememberMeManager.class, rememberMeManager);
	}
*/
}
